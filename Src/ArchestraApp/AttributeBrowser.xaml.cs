﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace ArchestrA.Apps.AttributeBrowser
{
    public partial class aaAttributeBrowserApp : UserControl
    {
        private bool _loaded = false;

        public bool AutoFetch
        {
            get { return _model.AutoFetch; }
            set { _model.AutoFetch = value; }
        }

        public String Server
        {
            get
            {
                return _model.Server;
            }
            set
            {
                _model.Server = value;
            }
        }
        public int RefreshInterval
        {
            get
            {
                return _model.RefreshInterval;
            }
            set
            {
                _model.RefreshInterval = value;
            }
        }
        public bool AutoRefresh
        {
            get
            {
                return _model.AutoRefresh;
            }
            set
            {
                _model.AutoRefresh = value;
            }
        }

        public aaAttributeBrowserApp()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                ArchestrA.Client.MyViewApp.Navigation.PropertyChanged += Navigation_PropertyChanged;
            }
            catch { }
            ((INotifyCollectionChanged)listView.Items).CollectionChanged += AaAttributeBrowserApp_CollectionChanged;
            _model.LogDebugInfo = false;
            _model.ObjectName = Client.MyViewApp.Navigation.CurrentAsset;
            _loaded = true;
        }

        private void AaAttributeBrowserApp_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                foreach (GridViewColumn c in gridView.Columns)
                {
                    if (c.Width.Equals(double.NaN))
                    {
                        c.Width = 0;
                        c.Width = double.NaN;
                    }
                }
            });
        }

        private void Navigation_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(Client.MyViewApp.Navigation.CurrentAsset))
            {
                _model.ObjectName = Client.MyViewApp.Navigation.CurrentAsset;
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                try
                {
                    ArchestrA.Client.MyViewApp.Navigation.PropertyChanged -= Navigation_PropertyChanged;
                }
                catch { }
                _model.Dispose();
            }
            catch { }
        }

        private void LogInfo(string message)
        {
            if (_model.LogDebugInfo)
                File.AppendAllText("C:\\AttributeBrowserClientLog.txt", string.Format("{0} {1}: {2}{3}", this.GetHashCode().ToString(), DateTime.Now.ToString(), message, Environment.NewLine));
        }
    }
}
