﻿namespace aaAttributeBrowser
{
    partial class aaAttributeBrowserControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mnuPopup = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuShowObjectName = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAttributeName = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAttributeDescription = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.txtStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.pnlLoading = new System.Windows.Forms.Panel();
            this.lblStatus = new System.Windows.Forms.Label();
            this.loadingCircle1 = new MRG.Controls.UI.LoadingCircle();
            this.mnuPopup.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.pnlLoading.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.Name = "listView1";
            this.listView1.ShowGroups = false;
            this.listView1.Size = new System.Drawing.Size(722, 417);
            this.listView1.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 150;
            // 
            // columnHeader2
            // 
            this.columnHeader2.DisplayIndex = 2;
            this.columnHeader2.Text = "Value";
            this.columnHeader2.Width = 100;
            // 
            // columnHeader3
            // 
            this.columnHeader3.DisplayIndex = 1;
            this.columnHeader3.Text = "Description";
            this.columnHeader3.Width = 200;
            // 
            // mnuPopup
            // 
            this.mnuPopup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuShowObjectName,
            this.mnuAttributeName,
            this.mnuAttributeDescription});
            this.mnuPopup.Name = "mnuPopup";
            this.mnuPopup.Size = new System.Drawing.Size(185, 70);
            // 
            // mnuShowObjectName
            // 
            this.mnuShowObjectName.Checked = true;
            this.mnuShowObjectName.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mnuShowObjectName.Name = "mnuShowObjectName";
            this.mnuShowObjectName.Size = new System.Drawing.Size(184, 22);
            this.mnuShowObjectName.Text = "Show Object Name";
            this.mnuShowObjectName.Click += new System.EventHandler(this.mnuShowObjectName_Click);
            // 
            // mnuAttributeName
            // 
            this.mnuAttributeName.Checked = true;
            this.mnuAttributeName.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mnuAttributeName.Name = "mnuAttributeName";
            this.mnuAttributeName.Size = new System.Drawing.Size(184, 22);
            this.mnuAttributeName.Text = "Attribute Name";
            this.mnuAttributeName.Click += new System.EventHandler(this.mnuAttributeName_Click);
            // 
            // mnuAttributeDescription
            // 
            this.mnuAttributeDescription.Checked = true;
            this.mnuAttributeDescription.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mnuAttributeDescription.Name = "mnuAttributeDescription";
            this.mnuAttributeDescription.Size = new System.Drawing.Size(184, 22);
            this.mnuAttributeDescription.Text = "Attribute Description";
            this.mnuAttributeDescription.Click += new System.EventHandler(this.mnuAttributeDescription_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 417);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(723, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // txtStatusLabel
            // 
            this.txtStatusLabel.Name = "txtStatusLabel";
            this.txtStatusLabel.Size = new System.Drawing.Size(51, 17);
            this.txtStatusLabel.Text = "Success.";
            // 
            // pnlLoading
            // 
            this.pnlLoading.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlLoading.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLoading.Controls.Add(this.loadingCircle1);
            this.pnlLoading.Controls.Add(this.lblStatus);
            this.pnlLoading.Location = new System.Drawing.Point(272, 186);
            this.pnlLoading.Name = "pnlLoading";
            this.pnlLoading.Size = new System.Drawing.Size(204, 39);
            this.pnlLoading.TabIndex = 3;
            this.pnlLoading.Visible = false;
            this.pnlLoading.VisibleChanged += new System.EventHandler(this.pnlLoading_VisibleChanged);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(57, 7);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(128, 23);
            this.lblStatus.TabIndex = 0;
            this.lblStatus.Text = "Fetching data...";
            // 
            // loadingCircle1
            // 
            this.loadingCircle1.Active = false;
            this.loadingCircle1.Color = System.Drawing.Color.DarkGray;
            this.loadingCircle1.InnerCircleRadius = 5;
            this.loadingCircle1.Location = new System.Drawing.Point(8, 1);
            this.loadingCircle1.Name = "loadingCircle1";
            this.loadingCircle1.NumberSpoke = 12;
            this.loadingCircle1.OuterCircleRadius = 11;
            this.loadingCircle1.RotationSpeed = 100;
            this.loadingCircle1.Size = new System.Drawing.Size(42, 36);
            this.loadingCircle1.SpokeThickness = 2;
            this.loadingCircle1.StylePreset = MRG.Controls.UI.LoadingCircle.StylePresets.MacOSX;
            this.loadingCircle1.TabIndex = 4;
            this.loadingCircle1.Text = "loadingCircle1";
            // 
            // aaAttributeBrowserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.pnlLoading);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.listView1);
            this.Name = "aaAttributeBrowserControl";
            this.Size = new System.Drawing.Size(723, 439);
            this.Load += new System.EventHandler(this.aaAttributeBrowserControl_Load);
            this.mnuPopup.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.pnlLoading.ResumeLayout(false);
            this.pnlLoading.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ContextMenuStrip mnuPopup;
        private System.Windows.Forms.ToolStripMenuItem mnuShowObjectName;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel pnlLoading;
        private System.Windows.Forms.Label lblStatus;
        private MRG.Controls.UI.LoadingCircle loadingCircle1;
        private System.Windows.Forms.ToolStripStatusLabel txtStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem mnuAttributeName;
        private System.Windows.Forms.ToolStripMenuItem mnuAttributeDescription;
    }
}
