﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aaAttributeBrowser
{
    public class ArchestrAResizeEventArgs: EventArgs
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public ArchestrAResizeEventArgs(int width, int height)
        {
            Width = width;
            Height = height;
        }
    }
}
