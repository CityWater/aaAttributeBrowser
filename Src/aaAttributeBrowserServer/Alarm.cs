﻿namespace aaAttributeBrowser
{
    public  class Alarm
    {
        public string State { get; set; }
        public string Type { get; set; }
        public string TagName { get; set; }
        public string Group { get; set; }
        public string Guid { get; set; }
        public string Value { get; set; }
        public string Priority { get; set; }
        public string Description { get; set; }
        public string Time { get; set; }
        public string Operator { get; set; }
    }
}