﻿using ArchestrA.MxAccess;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace aaAttributeBrowser
{

    public class DeployedObject
    {
        public int? ID { get; set; }
        public string Name { get; set; }
        public string ParentTemplate { get; set; }
        public string Area { get; set; }
        public string BaseTemplate { get; set; }
        public string Description { get; set; }
    }


    public class DeployedObjects
    {
        List<DeployedObject> _objects = new List<DeployedObject>();
        int _count = 0;
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string GetAllObjects(string galaxyName)
        {
            QueryDatabase(galaxyName);
            foreach (var obj in _objects)
            {
                aaAttributeManager.Singleton.AddCustomObject(obj.Name + ".ShortDesc", ObjectCallback);
                _count++;
            }
            var t = DateTime.Now;

            while (_count > 0 && (DateTime.Now - t).Seconds < 10)
                continue;

            return JsonConvert.SerializeObject(_objects);
        }

        public void ObjectCallback(string thing)
        {
            var obj = aaAttributeManager.Singleton.FetchCustomObject(thing);
            //TODO: this is not fool proof, do it properly
            //still not done very well but, if we trim .ShortDesc off the end they should match
            var res = (from i in _objects where thing.Substring(0,thing.Length-10) == i.Name select i).FirstOrDefault();
            if (res != null)
            {
                res.Description = obj.ToString();
                _count--;
            }
        }

        public void QueryDatabase(string galaxyName)
        {
            _objects.Clear();
            //this gets all deployed objects, their parents, base templates and areas
            string sql = @"SELECT tObject.gobject_id
                            ,tObject.tag_name as ObjectName
                            ,tParent.tag_name as ParentTemplate
                            ,tTemplate.original_template_tagname as BaseTemplate
                            ,tArea.tag_name as Area
                            FROM gobject as tObject
                            join gobject as tParent on tObject.derived_from_gobject_id = tParent.gobject_id
                            join template_definition as tTemplate on tObject.template_definition_id = tTemplate.template_definition_id
                            join gobject as tArea on tObject.area_gobject_id = tArea.gobject_id
                            where tObject.deployed_package_id <> 0 order by Area";

            try
            {
                using (SqlConnection conn = new SqlConnection("server=192.168.31.128;uid=sa;pwd=abc123!@#;database=TT;"))
                {
                    conn.Open();
                    using (SqlCommand command = new SqlCommand(sql, conn))
                    {
                        using (IDataReader myReader = command.ExecuteReader())
                        {
                            while (myReader.Read())
                            {
                                DeployedObject o = new DeployedObject() { ID = myReader.GetValue(0) as int?, Name = myReader.GetValue(1) as string, ParentTemplate = myReader.GetValue(2) as string, BaseTemplate = myReader.GetValue(3) as string, Area = myReader.GetValue(4) as string };
                                _objects.Add(o);
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                _log.Error(ex.ToString());
            }
        }
    }
}
