﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace aaAttributeBrowser
{
    public class aaObject
    {
        public string Name { get; set; }
        public List<aaAttribute> Attributes { get; set; }

        [JsonIgnore]
        public DateTime LastAccessed { get; set; } = DateTime.Now;
        [JsonIgnore]
        public bool DoesntExist { get; set; } = false;

    }

    public class aaAttribute
    {
        public string Name { get; set; }
        public string Value
        {
            get
            {
                Type t = _value.GetType();
                if (t == typeof(bool))
                {
                    if ((bool)_value)
                    {
                        return OnLabel;
                    }
                    else
                    {
                        return OffLabel;
                    }
                }
                if (t == typeof(int))
                {
                    return _value.ToString() + " " + EngUnit;
                }
                if (t == typeof(float))
                {
                    double v = ((float)_value);
                    return Math.Round(v, 2).ToString() + " " + EngUnit;
                }
                if (t == typeof(double))
                {
                    return Math.Round((double)_value, 2).ToString() + " " + EngUnit;
                    
                }
                return _value.ToString();
            }

            set
            {
                _value = value;
            }
        }
        public string Description { get; set; }
        public int Quality { get; set; }
        public bool InAlarm { get; set; }
        public DateTime TimeStamp { get; set; }

        [JsonIgnore]
        public string OnLabel { get; set; }
        [JsonIgnore]
        public string OffLabel { get; set; }
        [JsonIgnore]
        public Type Type { get; set; }
        [JsonIgnore]
        public string EngUnit { get; set; }

        private object _value;

        public aaAttribute()
        {
            OnLabel = "True";
            OffLabel = "False";
        }

        public void SetValue(object obj)
        {
            _value = obj;
        }

    }

    public enum aaLMXValuePurpose
    {
        AttributeList,
        OnLabel,
        OffLabel,
        Description,
        EngUnit,
        Normal,
        InAlarm,
        Custom
    }

    public class LMXValue
    {
        public string Name { get; set; }
        //Get the value once then unadvise it
        public bool OnceOnly { get; set; }
        public aaObject Parent { get; set; }
        public aaLMXValuePurpose Purpose { get; set; }
        public aaAttribute Attribute { get; set; }
        public Action<string> CustomCallback { get; set; }
    }

}
