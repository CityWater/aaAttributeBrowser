﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;

namespace aaAttributeBrowser
{
    public class Options
    {
        [Option('p', HelpText = "Port to listen for requests on", DefaultValue =8123)]
        public int ListenPort { get; set; }

        [Option('s', HelpText = "HTTPS mode", DefaultValue =false)]
        public bool Https { get; set; }

        [Option('a', HelpText = "Autostart", DefaultValue = false)]
        public bool Autostart { get; set; }


        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this, (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}

