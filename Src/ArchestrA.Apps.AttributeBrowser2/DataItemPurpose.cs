﻿using ArchestrA.Client.RuntimeData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace ArchestrA.Apps.AttributeBrowser2
{
    public enum DataItemPurpose
    {
        AttributeList,
        OnLabel,
        OffLabel,
        Description,
        EngUnit,
        Normal,
        InAlarm,
        Value,
        Status,
        StatusAndValue,
    }
}