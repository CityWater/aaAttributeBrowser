﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ArchestrA.Apps.AttributeBrowser2
{
    public class aaAttribute : INotifyPropertyChanged
    {
        private string _name;
        private string _description;
        private int _quality;
        private bool _inAlarm;
        private DateTime _timeStamp;
        private aaObject _parentObject;
        private Type _type = typeof(string);
        private string _engUnit;
        private object _value = "";

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnProptertyChanged();
            }
        }
        public string Value
        {
            get
            {
                Type t = _value.GetType();
                if (t == typeof(bool))
                {
                    return _value.ToString();
                    /*
                    if ((bool)_value)
                    {
                        return OnLabel;
                        
                    }
                    else
                    {
                        return OffLabel;
                    }
                    */
                }
                if (t == typeof(int))
                {
                    return _value.ToString() + " " + EngUnit;
                }
                if (t == typeof(float))
                {
                    double v = ((float)_value);
                    return Math.Round(v, 2).ToString() + " " + EngUnit;
                }
                if (t == typeof(double))
                {
                    return Math.Round((double)_value, 2).ToString() + " " + EngUnit;

                }
                return _value.ToString();
            }

            set
            {
                _value = value;
                OnProptertyChanged();
            }
        }
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                OnProptertyChanged();
            }
        }
        public int Quality
        {
            get
            {
                return _quality;
            }
            set
            {
                _quality = value;
                OnProptertyChanged();
            }
        }
        public bool InAlarm
        {
            get
            {
                return _inAlarm;
            }
            set
            {
                _inAlarm = value;
                OnProptertyChanged();
            }
        }
        public DateTime TimeStamp
        {
            get
            {
                return _timeStamp;
            }
            set
            {
                _timeStamp = value;
                OnProptertyChanged();
            }
        }
        public aaObject ParentObject
        {
            get
            {
                return _parentObject;
            }
            set
            {
                _parentObject = value;
                OnProptertyChanged();
            }
        }
        public Type Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
                OnProptertyChanged();
            }
        }
        public string EngUnit
        {
            get
            {
                return _engUnit;
            }
            set
            {
                _engUnit = value;
                OnProptertyChanged();
            }
        }

        public override string ToString()
        {
            try
            {
                return Name + ":" + Value;
            }
            catch
            {
                return base.ToString();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Filter Flags { get; set; } = 0;

        public List<DataItem> DataItems { get; set; } = new List<DataItem>();

        public aaAttribute(string name, List<string> items, aaObject parent)
        {
            Name = name;
            ParentObject = parent;
            string a;
            if (items.Contains(name + ".InputSource"))
            {
                Flags = Flags | Filter.HasInput;
            }
            if (items.Contains(name + ".OutputDest"))
            {
                Flags = Flags | Filter.HasOutput;
            }
            if (name.StartsWith("Calc."))
            {
                Flags = Flags | Filter.IsCalculation;
            }
            if (name.EndsWith(".PV"))
            {
                Flags = Flags | Filter.IsProcessValue;
            }
            if (name.StartsWith("Status."))
            {
                Flags = Flags | Filter.IsStatus;
            }
            if (name.StartsWith("Cfg."))
            {
                Flags = Flags | Filter.IsConfiguration;
            }
            if (name.StartsWith("Cmd."))
            {
                Flags = Flags | Filter.IsCommand;
            }
            if (name.StartsWith("_"))
            {
                Flags = Flags | Filter.IsHidden;
            }
            if (items.Contains(a = name + ".Description"))
            {
                AddDataItem(ParentObject.Name + "." + a, DataItemPurpose.Description);
            }
            if (items.Contains(a = name + ".Msg"))
            {
                AddDataItem(ParentObject.Name + "." + a, DataItemPurpose.Value);
                AddDataItem(ParentObject.Name + "." + name, DataItemPurpose.Status);
            }
            else
            {
                AddDataItem(ParentObject.Name + "." + name, DataItemPurpose.StatusAndValue);
            }
            if (items.Contains(a = name + ".EngUnits"))
            {
                AddDataItem(ParentObject.Name + "." + a, DataItemPurpose.EngUnit);
            }
            if (items.Contains(a = name + ".InAlarm"))
            {
                AddDataItem(ParentObject.Name + "." + a, DataItemPurpose.InAlarm);
            }
            if (items.Contains(a = name + ".AlarmMostUrgentInAlarm"))
            {
                AddDataItem(ParentObject.Name + "." + a, DataItemPurpose.InAlarm);
            }
        }

        private void AddDataItem(string reference, DataItemPurpose purpose)
        {
            DataItem d = new DataItem();
            d.ReferenceString = reference;
            d.Purpose = purpose;
            d.OwningObject = ParentObject.Name;
            d.PropertyChanged += DataItem_PropertyChanged;
            DataItems.Add(d);
        }

        private void DataItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var v = sender as DataItem;
            if (v.DataValue == null)
                return;
            switch (v.Purpose)
            {
                case DataItemPurpose.Description:
                    Description = v.DataValue.ToString();
                    break;
                case DataItemPurpose.EngUnit:
                    EngUnit = v.DataValue.ToString();
                    break;
                case DataItemPurpose.InAlarm:
                    InAlarm = (bool)v.DataValue;
                    break;
                case DataItemPurpose.Value:
                    Type = v.DataValue.GetType();
                    _value = v.DataValue;
                    OnProptertyChanged(nameof(Value));
                    TimeStamp = v.Timestamp;
                    break;
                case DataItemPurpose.StatusAndValue:
                    Type = v.DataValue.GetType();
                    _value = v.DataValue;
                    OnProptertyChanged(nameof(Value));
                    TimeStamp = v.Timestamp;
                    Quality = v.MxQuality;
                    break;
                case DataItemPurpose.Status:
                    Quality = v.MxQuality;
                    break;
            }
        }

        private void OnProptertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
