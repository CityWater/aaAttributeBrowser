﻿using ArchestrA.Client.RuntimeData;
using System;
using System.Collections.Specialized;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ArchestrA.Apps.AttributeBrowser
{
    public partial class aaAttributeBrowserApp2 : UserControl, IRuntimeDataClient
    {
        private bool _loaded = false;


        private DataSubscription _dataSubscription;

        /// <summary>
        /// Gets or sets the DataSubscription instance
        /// Framework will set the value when the control is loaded in View
        /// </summary>
        public DataSubscription DataSubscription
        {
            get
            {
                return _dataSubscription;
            }

            set
            {
                _dataSubscription = value;

                // Push the DataSubscription to view model
                _model.DataSubscription = value;
            }
        }

        public aaAttributeBrowserApp2()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                ArchestrA.Client.MyViewApp.Navigation.PropertyChanged += Navigation_PropertyChanged;
            }
            catch { }
            //((INotifyCollectionChanged)listView.Items).CollectionChanged += AaAttributeBrowserApp_CollectionChanged;
            _model.Name  = Client.MyViewApp.Navigation.CurrentAsset;
            _model.DescriptionUpdated += _model_DescriptionUpdated;

            _model.SubscribeAttributes();
            _loaded = true;
        }

        private void _model_DescriptionUpdated()
        {
            Task ignoredAwaitableResult = AutoSizeListView();
        }

        private void Navigation_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(Client.MyViewApp.Navigation.CurrentAsset))
            {
                _model.Unsubscribe();
                _model.Name =  Client.MyViewApp.Navigation.CurrentAsset;
                _model.SubscribeAttributes();
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                try
                {
                    ArchestrA.Client.MyViewApp.Navigation.PropertyChanged -= Navigation_PropertyChanged;
                }
                catch { }
                //doesn't seem to be much point in this as it is too late
                //_model.Unsubscribe();
            }
            catch { }
        }

        private void LogInfo(string message)
        {
            //if (_model.LogDebugInfo)
              //  File.AppendAllText("C:\\AttributeBrowserClientLog.txt", string.Format("{0} {1}: {2}{3}", this.GetHashCode().ToString(), DateTime.Now.ToString(), message, Environment.NewLine));
        }

        private async Task AutoSizeListView()
        {
            //this is a dirt hack - need to work out how to do it properly
            await Task.Delay(100);
            Dispatcher.Invoke(() =>
            {
                GridViewColumn desc = null;
                double sum = 0;
                foreach (GridViewColumn c in gridView.Columns)
                {
                    var ch = c.Header as GridViewColumnHeader;
                    if (ch?.Content?.ToString() == "Description")
                    {
                        desc = c;
                    }
                    else if (c.Width.Equals(double.NaN))
                    {
                        c.Width = c.ActualWidth;
                        c.Width = double.NaN;
                        sum += c.ActualWidth;
                    }
                    else
                    {
                        sum += c.ActualWidth;
                    }                       
                }
                if (desc != null)
                    desc.Width = listView.ActualWidth - sum - SystemParameters.VerticalScrollBarWidth;               
            });
            await Task.Delay(100);
            Dispatcher.Invoke(() =>
            {
                GridViewColumn desc = null;
                double sum = 0;
                foreach (GridViewColumn c in gridView.Columns)
                {
                    var ch = c.Header as GridViewColumnHeader;
                    if (ch?.Content?.ToString() == "Description")
                    {
                        desc = c;
                    }
                    else if (c.Width.Equals(double.NaN))
                    {
                        c.Width = c.ActualWidth;
                        c.Width = double.NaN;
                        sum += c.ActualWidth;
                    }
                    else
                    {
                        sum += c.ActualWidth;
                    }
                }
                if (desc != null)
                    desc.Width = listView.ActualWidth - sum - SystemParameters.VerticalScrollBarWidth;
            });
        }
    }
}
