﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aaAttributeBrowser
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnFetch_Click(object sender, EventArgs e)
        {
            aaAttributeBrowser.GetNewAttributes(txtObject.Text);
        }

        private void txtServer_TextChanged(object sender, EventArgs e)
        {
            aaAttributeBrowser.Server = txtServer.Text;
        }

        private void btnRefetch_Click(object sender, EventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            aaAttributeBrowser.StopTimer();
        }
    }
}
