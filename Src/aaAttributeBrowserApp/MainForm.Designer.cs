﻿namespace aaAttributeBrowser
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFetch = new System.Windows.Forms.Button();
            this.txtObject = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.btnRefetch = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.aaAttributeBrowser = new aaAttributeBrowser.aaAttributeBrowserControl();
            this.SuspendLayout();
            // 
            // btnFetch
            // 
            this.btnFetch.Location = new System.Drawing.Point(227, 9);
            this.btnFetch.Name = "btnFetch";
            this.btnFetch.Size = new System.Drawing.Size(75, 21);
            this.btnFetch.TabIndex = 1;
            this.btnFetch.Text = "Fetch";
            this.btnFetch.UseVisualStyleBackColor = true;
            this.btnFetch.Click += new System.EventHandler(this.btnFetch_Click);
            // 
            // txtObject
            // 
            this.txtObject.Location = new System.Drawing.Point(113, 11);
            this.txtObject.Name = "txtObject";
            this.txtObject.Size = new System.Drawing.Size(100, 21);
            this.txtObject.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "Object Instance";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(737, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "Server Root";
            // 
            // txtServer
            // 
            this.txtServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtServer.Location = new System.Drawing.Point(809, 11);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(188, 21);
            this.txtServer.TabIndex = 6;
            this.txtServer.Text = "http://127.0.0.1:8123/";
            this.txtServer.TextChanged += new System.EventHandler(this.txtServer_TextChanged);
            // 
            // btnRefetch
            // 
            this.btnRefetch.Enabled = false;
            this.btnRefetch.Location = new System.Drawing.Point(308, 9);
            this.btnRefetch.Name = "btnRefetch";
            this.btnRefetch.Size = new System.Drawing.Size(75, 21);
            this.btnRefetch.TabIndex = 7;
            this.btnRefetch.Text = "Refetch";
            this.btnRefetch.UseVisualStyleBackColor = true;
            this.btnRefetch.Click += new System.EventHandler(this.btnRefetch_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.Location = new System.Drawing.Point(389, 9);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 21);
            this.btnDelete.TabIndex = 8;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(470, 9);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 21);
            this.btnStop.TabIndex = 9;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // aaAttributeBrowser
            // 
            this.aaAttributeBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aaAttributeBrowser.AutoSizeParent = false;
            this.aaAttributeBrowser.Location = new System.Drawing.Point(12, 36);
            this.aaAttributeBrowser.MaxHeight = 800;
            this.aaAttributeBrowser.MaxWidth = 1000;
            this.aaAttributeBrowser.Name = "aaAttributeBrowser";
            this.aaAttributeBrowser.Server = "http://127.0.0.1:8123/";
            this.aaAttributeBrowser.ShowAttributeDescription = true;
            this.aaAttributeBrowser.ShowAttributeName = false;
            this.aaAttributeBrowser.ShowObjectName = false;
            this.aaAttributeBrowser.Size = new System.Drawing.Size(988, 424);
            this.aaAttributeBrowser.TabIndex = 4;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 462);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnRefetch);
            this.Controls.Add(this.txtServer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.aaAttributeBrowser);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtObject);
            this.Controls.Add(this.btnFetch);
            this.Name = "MainForm";
            this.Text = "aaAttributeBrowserTester";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnFetch;
        private System.Windows.Forms.TextBox txtObject;
        private System.Windows.Forms.Label label1;
        private aaAttributeBrowserControl aaAttributeBrowser;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.Button btnRefetch;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnStop;
    }
}

