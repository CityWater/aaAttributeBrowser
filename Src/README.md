# aaAttributeBrowser

aaAttributeBrowser serves up data about ArchestrA objects, System Platform/InTouch Alarms, and other stuff that I deem useful.  It consists of a server component that runs on the Galaxy Node.  The data served can be put into three main buckets:
 * Alarms
 * Object attributes
 * Other stuff
 
### Alarms
Alarms intended to be viewed via a webpage, e.g. https://localhost:8123/Alarms/index.html.  This will present with a list of alarms that will update every 30 seconds.  Push notifications will be delivered if there are any unacknowledged alarms in the list.

### Object attributes

Object attribute data can be obtained by e.g. https://localhost:8123/object/{objectname}.  The server will only serve IO attributes at present.  Data is returned in JSON format.  This data can be consumed by the client control, which may be embedded into an ArchestrA object or into a custom application. An example application is provided.

### Server Usage
The server supports a couple of runtime arguments
 * p - specify a default port to listen on
 * s - start in https mode by default
 * a - start listening on program start

### Using SSL
SSL is required for push notifications to work on Chrome on Android (and possibly others that I haven't tested).
If you choose to use ssl, then you must have an appropriate certificate installed into the local machine certificate store.  You must also add a urlacl and sslcert using netsh.  For example:
```
netsh http add urlacl url=https://+:8123/ user=\Everyone
netsh http add sslcert ipport=0.0.0.0:8123 certhash=yourcerthashwithnospaceshere appid='{your-guid-here}'
```
### Building

To build this solution you will need:
 * The MXAccess Toolkit
 * An ArchestrA Node with Visual Studio 2015 OR a copy of ArchestrA.MXAccess.dll and ArchestrA.Core.dll

There are 5 projects in the solution:
 * aaAttributeBrowserControl - client control for embedding in ArchestrA (or elsewhere)
 * aaAttributeBrowserApp - app for testing client control
 * aaAttributeBrowserServer - server component
 * aaAttributeBrowserClient - client component
 * Archestra.Apps.AttributeBrowser - InTouch OMI App
 
### Using the Server Component

The server component can either be deployed as an ArchestrA object or run standalone, on any platform in the galaxy.  As a standlone program it can take an optional argument on the command line for the listening port, in which case it will start listening straight away.  If you omit the argument, it will not start until you tell it to using the UI.  You can deploy using an archestra object using a technique similar to https://wonderwarewest.com/download/Wonderware%20Tech%20Notes/0398%20Starting%20and%20Stopping%20an%20Application%20or%20Service%20from%20an%20IAS%20Object.pdf.  

### Using the .NET Forms Control

The old client component (aaAttributeBrowser.dll) can be imported into a Galaxy as a client control.  The easiest way to use the control is to create a symbol (say aaAttributeBrowser), and embed the client control into it.  Create a custom property (pObject) to reference the object you want to display, and in the OnShow script:

```
aaAttributeBrowserControl1.Server = "http://yourserverip:port/;
aaAttributeBrowserControl1.GetNewAttributes(pObject);
```

When importing a new version of the client control, you may find the following procedure helpful:
* Delete the old control from the graphic toolbox.
* Import the new control
* Restart the IDE.  You can check the new version is correct by right clicking the control in the graphic toolbox and selecting properties.
* Go into the AttributeBrowser symbol, delete the control, save and close.
* Go into the AttributeBrowser symbol, and re-add the control, save and close.
* Done!

### Using the OMI App

Copy ArchestrA.Apps.AttributeBrowser, Netwonsoft.JSON and aaAttributeBrowserClient.dll files in to a new folder.  Import this into the IDE using the Import->ArchestrA Apps menu item.  Configure the apps properties after you have embedded it into a layout.
